# MPExitNotification
#### If user exit application has been notification;
#### 用户退出App会收到通知

## Description 描述
* 理论支持iOS8以上(亲测iOS9.3.3,iOS10.3)，应用退出时在通知中心收到应用退出消息

<!-- ![](http://7xp3rr.com1.z0.glb.clouddn.com/MPExitNotification.gif) -->
[查看演示效果](http://oa10sjuog.bkt.clouddn.com/mv.m4v)

## Usage 使用方法
### 第一步 
* 将子文件夹Classes拖入到项目中。
* 下载[MSWeakTimer](https://github.com/mindsnacks/MSWeakTimer)导入项目中 
* 使用CocoaPods 请在Podfile里面添加pod 'MSWeakTimer' 

### 第二步 
* 在AppDelegate.m中导入MPExitNotification.h
```objc
    #import "MPExitNotification.h"
```
### 第三步 在didFinishLaunchingWithOptions:中调用
```objc
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    MPExitNotification * exitNotification = [MPExitNotification shareInstance];
    // 文字描述
    exitNotification.body = @"您的应用已经关闭";
    // 如果你的app有开启定位 音乐播放等常驻后台的服务设置isBackground = Yes;
    exitNotification.isBackground = NO;
    // 开始执行
    [exitNotification execute];
    // 取消执行
    //[exitNotification cancelExecute];
    return YES;
}
```

## 联系方式:
* QQ : 351420450
* Email : mopellet@foxmail.com
* Resume : [个人简历](http://pkwans.com/about/)
