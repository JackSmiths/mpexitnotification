//
//  MPExitNotification.m
//  MPExitNotification
//
//  Created by mopellet on 2017/5/26.
//  Copyright © 2017年 mopellet. All rights reserved.
//

#import "MPExitNotification.h"
#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>
#import "MSWeakTimer.h"
#define IS_IOS_10_LATER    floorf([[UIDevice currentDevice].systemVersion floatValue]) >= 10.0 ? 1 : 0
#define IS_IOS_8_LATER     floorf([[UIDevice currentDevice].systemVersion floatValue]) >= 8.0 ? 1 : 0

/** 延迟多少秒执行 亲测至少大于2s才完美*/
const NSTimeInterval MPExitNotificationDelay = 1.2f;

#define MPApplication [UIApplication sharedApplication]
@interface MPExitNotification()
{
    UIBackgroundTaskIdentifier taskId;
}
@property (nonatomic, readwrite, strong) NSNotificationCenter *mp_NotificationCenter;
@property (nonatomic, readwrite, strong) MSWeakTimer *backgroundTimer;
@property (nonatomic, readwrite, strong) dispatch_queue_t privateQueue;
@property (nonatomic, readwrite, strong) UNMutableNotificationContent *mp_Content;
@property (nonatomic, readwrite, strong) UNTimeIntervalNotificationTrigger *mp_Trigger;
@property (nonatomic, readwrite, strong) UNNotificationRequest *mp_Request;
@property (nonatomic, readwrite, strong) UILocalNotification *mp_LocalNotification;
@property (nonatomic, readwrite, assign) BOOL isExecute;
@property (nonatomic, readonly, getter=isEnableNotification)  BOOL enableNotification;
@property (nonatomic, copy) void (^block)();
@end

@implementation MPExitNotification

NSString *requertIdentifier = @"mp_RequestIdentifier";

+ (instancetype)shareInstance
{
    static id instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[[self class] alloc] init];
    });
    
    return instance;
}

- (void)dealloc{
    [_mp_NotificationCenter removeObserver:self];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _mp_NotificationCenter = [NSNotificationCenter defaultCenter];
        MPApplication.applicationIconBadgeNumber = 0;
        _privateQueue = dispatch_queue_create("com.mindsnacks.private_queue", DISPATCH_QUEUE_CONCURRENT);
        [self requestAuthorization];
        [self initNotificationParameters];
    }
    return self;
}

- (void)initNotificationParameters {
        [self setParameterTitle:@""
                       subtitle:@""
                           body:@"Your application has been shut down;re-open it to resume access to your locks"];
}

#pragma mark - request authorization
- (void)requestAuthorization{
    
    if (IS_IOS_10_LATER) {
        //请求获取通知权限（角标，声音，弹框）
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];

        [center requestAuthorizationWithOptions:(UNAuthorizationOptionAlert | UNAuthorizationOptionBadge | UNAuthorizationOptionSound) completionHandler:^(BOOL granted, NSError * _Nullable error) {
            if (granted) {
                //获取用户是否同意开启通知
                NSLog(@"request authorization successed!");
            } else {
                // 点击不允许
                NSLog(@"request authorization fail!");
            }
        }];
    }else if (IS_IOS_8_LATER){
        //iOS8 - iOS10
        [MPApplication registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeSound | UIUserNotificationTypeBadge categories:nil]];
        
    }
//    else if ([[UIDevice currentDevice].systemVersion floatValue] < 8.0) {
//       // iOS8系统以下
//        [application registerForRemoteNotificationTypes:UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound];
//    }
    
}

#pragma mark Getter && Setter


- (void)setParameterTitle:(NSString *)title subtitle:(NSString *)subtitle body:(NSString *)body{
    _body = body;
    _title = title;
    _subtitle = subtitle;
    if(IS_IOS_10_LATER) {
        if (!_mp_Content) {
            _mp_Content = [[UNMutableNotificationContent alloc] init];
        }
        _mp_Content.sound = [UNNotificationSound defaultSound];
        _mp_Content.title = title;
        _mp_Content.subtitle = subtitle;
        _mp_Content.body = body;
        _mp_Trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:MPExitNotificationDelay repeats:NO];
        _mp_Request = [UNNotificationRequest
                       requestWithIdentifier:requertIdentifier
                       content:_mp_Content
                       trigger:_mp_Trigger];
    }
    else if (IS_IOS_8_LATER){
        //定义本地通知对象
        if (!_mp_LocalNotification) {
            _mp_LocalNotification = [UILocalNotification new];
        }
        _mp_LocalNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:MPExitNotificationDelay];
        _mp_LocalNotification.timeZone = [NSTimeZone systemTimeZone];
        _mp_LocalNotification.alertBody = _body;
        _mp_LocalNotification.alertTitle = _title;
        _mp_LocalNotification.soundName = UILocalNotificationDefaultSoundName;
    }
}



- (void)setBody:(NSString *)body{
    _body = body;
    [self setParameterTitle:_title subtitle:_subtitle body:_body];
}

- (void)setTitle:(NSString *)title {
    _title = title;
    [self setParameterTitle:_title subtitle:_subtitle body:_body];
}

- (void)setSubtitle:(NSString *)subtitle {
    _subtitle = subtitle;
    [self setParameterTitle:_title subtitle:_subtitle body:_body];
}

- (BOOL)isEnableNotification{
    return MPApplication.currentUserNotificationSettings.types != UIUserNotificationTypeNone;
}

- (void)execute{
    
    [_mp_NotificationCenter addObserver:self
                           selector:@selector(mp_applicationDidEnterBackground:)
                               name:UIApplicationDidEnterBackgroundNotification
                             object:MPApplication];
    
    [_mp_NotificationCenter addObserver:self
                               selector:@selector(mp_applicationDidBecomeActive:)
                                   name:UIApplicationDidBecomeActiveNotification
                                 object:MPApplication];
    
    [_mp_NotificationCenter addObserver:self
                               selector:@selector(mp_applicationWillTerminate:)
                                   name:UIApplicationWillTerminateNotification
                                 object:MPApplication];
    
    [_mp_NotificationCenter addObserver:self
                               selector:@selector(mp_applicationDidReceiveMemoryWarning:)
                                   name:UIApplicationDidReceiveMemoryWarningNotification
                                 object:MPApplication];
    _isExecute = YES;
}

- (void)cancelExecute{
    [_backgroundTimer invalidate];
    _backgroundTimer = nil;
    _isExecute = NO;
    [self removeNotification];
    [_mp_NotificationCenter removeObserver:self];
    [self endBack];
}

- (void)mp_monitor:(void (^)())block
{
    self.block = block;
}


#pragma mark NSNotificationCenter Actions
- (void)mp_applicationDidEnterBackground:(NSNotification *)notification {
//    if (_isBackground) {
//        return;
//    }
    NSLog(@"%s",__func__);
    
    [self beginTask];
}
//
- (void)mp_applicationDidBecomeActive:(NSNotification *)notification
{
    NSLog(@"%s",__func__);
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];//进入前台取消应用消息图标
//    [[UIApplication sharedApplication] endBackgroundTask:taskId];
//    taskId = UIBackgroundTaskInvalid;
    [self endBack];
    
//    if (_backgroundTimer) {
//        return;
//    }
//    _backgroundTimer = [MSWeakTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(mp_timerWork:) userInfo:nil repeats:YES dispatchQueue:_privateQueue];
}


- (void)mp_applicationWillTerminate:(NSNotification *)notification
{

}

- (void)mp_applicationDidReceiveMemoryWarning:(NSNotification *)notification {
    [self cancelExecute];
}

- (void)mp_timerWork:(NSTimer *)timer {

    if (!self.isEnableNotification) {
        [self removeNotification];
        return; // 禁用就不执行
    }
    
    if ([[UIApplication sharedApplication] backgroundTimeRemaining] < 10) {
        [self endBack];
    }
    
    if (self.block) {
        self.block();
    }
    
    if (IS_IOS_10_LATER) {
        [self removeNotification];
        //将通知加到通知中心
        [[UNUserNotificationCenter currentNotificationCenter] addNotificationRequest:_mp_Request withCompletionHandler:^(NSError * _Nullable error) {
            if (error) {
                NSLog(@"Error:%@",error);
            }
        }];
    }
    else if (IS_IOS_8_LATER){
        [self addLocalNotification];
    }
    else{
        if (_backgroundTimer) {
            [_backgroundTimer invalidate];
            _backgroundTimer = nil;
        }
    }
}


#pragma mark - 私有方法

- (void)beginTask
{
//    taskId = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
//        
//        [self endBack]; // 如果在系统规定时间内任务还没有完成，在时间到之前会调用到这个方法，一般是10分钟
//    }];
//
    
    BOOL backgroundAccepted = [[UIApplication sharedApplication] setKeepAliveTimeout:600 handler:^{
        [self backgroundHandler];
    }];
    
    if (backgroundAccepted)
    {
        NSLog(@"backgrounding accepted");
    }
    
    [self backgroundHandler];
}


- (void)endBack
{
     NSLog(@"### -->endBack");
    [_backgroundTimer invalidate];
    _backgroundTimer = nil;
    [self removeNotification];
    [[UIApplication sharedApplication] endBackgroundTask:taskId];
    taskId = UIBackgroundTaskInvalid;
}

- (void)backgroundHandler {
    NSLog(@"### -->backgroundinghandler");

    taskId = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        [self endBack];
    }];
    
    if (_backgroundTimer) {
        return;
    }
    _backgroundTimer = [MSWeakTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(mp_timerWork:) userInfo:nil repeats:YES dispatchQueue:_privateQueue];
    
}


#pragma mark 添加本地通知
-(void)addLocalNotification {
    [self removeNotification];
    
    _mp_LocalNotification = [UILocalNotification new];
    _mp_LocalNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:MPExitNotificationDelay];
    _mp_LocalNotification.timeZone = [NSTimeZone systemTimeZone];
    _mp_LocalNotification.alertBody = _body;
    _mp_LocalNotification.alertTitle = _title;
    _mp_LocalNotification.soundName = UILocalNotificationDefaultSoundName;
    [MPApplication scheduleLocalNotification:_mp_LocalNotification];
}
#pragma mark 移除本地通知，在不需要此通知时记得移除
-(void)removeNotification{
    if (IS_IOS_10_LATER && _mp_Request) {
        [[UNUserNotificationCenter currentNotificationCenter] removePendingNotificationRequestsWithIdentifiers:@[requertIdentifier]];
    }
    else if (IS_IOS_8_LATER && _mp_LocalNotification) {
        [MPApplication cancelLocalNotification:_mp_LocalNotification];
    }
}

@end
