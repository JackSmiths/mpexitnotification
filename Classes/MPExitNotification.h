//
//  MPExitNotification.h
//  MPExitNotification
//
//  Created by mopellet on 2017/5/26.
//  Copyright © 2017年 mopellet. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MPExitNotification : NSObject

+ (instancetype)shareInstance;

@property (nonatomic, readwrite, copy) NSString *title;
@property (nonatomic, readwrite, copy) NSString *subtitle;
@property (nonatomic, readwrite, copy) NSString *body;
/** 该应用是否已经是后台运行的应用*/
@property (nonatomic, readwrite, assign) BOOL isBackground;

/**
 *  开始执行
 */
- (void)execute;

/**
 *  取消执行
 */
- (void)cancelExecute;

/** 监测执行的回调 测试专用*/
- (void)mp_monitor:(void (^)())block;

@end
