//
//  AppDelegate.h
//  MPExitNotification
//
//  Created by mopellet on 2017/5/26.
//  Copyright © 2017年 mopellet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

