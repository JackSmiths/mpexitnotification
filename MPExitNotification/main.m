//
//  main.m
//  MPExitNotification
//
//  Created by mopellet on 2017/5/26.
//  Copyright © 2017年 mopellet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
